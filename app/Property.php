<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Property extends Model
{
    protected $table = "properties";

    protected $fillable = [
    	'name',	'description',
    ];

    public function users(){
        return $this->hasMany('App\User', 'properties_id');
    }
}
