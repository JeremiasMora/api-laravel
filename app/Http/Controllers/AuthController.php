<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\User;


class AuthController extends Controller
{
    public function userAuth(Request $request){
    	$credentials = $request->only('email', 'password');
    	$token = null;

    	try {
    		if(!$token = JWTAuth::attempt($credentials)){
    			return response()->json(['error' => 'invalid_credentials']);
    		}
    	}catch (JWTException $e) {
    		return response()->json(['error' => 'somthing_went_wrong'], 500);
    	}

    	
    	$users = User::all();
    	foreach ($users as $user) {
    		if($token = JWTAuth::attempt($credentials)){
    			$user_id = $user->id;
    		}
    	}
    	//return User::findOrFail($user_id);
    	$usuario = User::find($user_id);
    	return response()->json([compact('token'), 'email' => $request->email, 'name' => $usuario->name, 'properties_id' => $usuario->properties_id]);	   	
    }
}
