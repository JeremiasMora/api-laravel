<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Property;


class PropertyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Property::all();

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    
        if(!is_array($request->all())){
            return ['error' => 'request must be an array'];
        }
        $rules = [
            'name' => 'required'
        ];
        try{

            $validator = \Validator::make($request->all(), $rules);

            if($validator->fails()){
                return [
                    'created' => false,
                    'errors'  => $validator->errors()->all()
                ];
            }else{               
                Property::create($request->all());
                return ['created' => true];
            }
        }catch (Exception $e){
            \Log::info('Error creating user: '.$e);
            return \Response::json(['created' => false], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            return Property::findOrFail($id);
        } catch (Exception $e) {
             $data = [
                'errors' => true,
                'msg' => $e->getMessage(),
            ];
            return \Response::json($data, 404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (!is_array($request->all())) {
            return ['error' => 'request must be an array'];
        }

         $rules = [
            'name'      => 'required'
        ];

        try {

            $validator = \Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                return [
                    'created' => false,
                    'errors'  => $validator->errors()->all()
                ];
            }else{

                $user = Property::findOrFail($id);
                $user->update($request->all());
                return ['updated' => true];
            }
            
        }catch (ModelNotFoundException $e) {
            $data = [
                'errors' => true,
                'msg' => $e->getMessage(),
            ];
            return \Response::json($data, 404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {            
            Property::destroy($id);
            return ['removed' => true];
        } catch (Exception $e) {
            $data = [
                'errors' => true,
                'msg' => $e->getMessage(),
            ];
            
            return \Response::json($data, 404);
        }
    }
}
