<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return User::with('property')->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(!is_array($request->all())){
            return ['error' => 'request must be an array'];
        }

        $rules = [
            'name' => 'required',
            'email'  => 'required|email',
            'password' => 'required',
            'properties_id' => 'required'
        ];

        $data = [
            'name' => $request->name,
            'email'  => $request->email,
            'password' => bcrypt($request->password),
            'properties_id' => $request->properties_id
        ];
        try{
            $validator = \Validator::make($data, $rules);
            if($validator->fails()){
                return [
                    'created' => false,
                    'errors'  => $validator->errors()->all()
                ];

            }else{
                User::create($data);
                return ['created' => true];
            }
            
        }catch (Exception $e){
            \Log::info('Error creating user: '.$e);
            return \Response::json(['created' => false], 500);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            return User::with('property')->where('id', $id)->first();
        } catch (Exception $e) {
             $data = [
                'errors' => true,
                'msg' => $e->getMessage(),
            ];
            return \Response::json($data, 404);
        }
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (!is_array($request->all())) {
            return ['error' => 'request must be an array'];
        }

         $rules = [
            'name'      => 'required',
            'email'     => 'required|email',
            'password'  => 'required',
            'properties_id' => 'required'
        ];

        $data = [
            'name' => $request->name,
            'email'  => $request->email,
            'password' => bcrypt($request->password),
            'properties_id' => $request->properties_id
        ];
        try {

            $validator = \Validator::make($data, $rules);
            if ($validator->fails()) {
                return [
                    'created' => false,
                    'errors'  => $validator->errors()->all()
                ];
            }else{

                $user = User::findOrFail($id);
                $user->update($data);
                return ['updated' => true];
            }
            
        }catch (ModelNotFoundException $e) {
            $data = [
                'errors' => true,
                'msg' => $e->getMessage(),
            ];
            return \Response::json($data, 404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {            
            User::destroy($id);
            return ['removed' => true];
        } catch (Exception $e) {
            $data = [
                'errors' => true,
                'msg' => $e->getMessage(),
            ];
            
            return \Response::json($data, 404);
        }
    }
}
